function input1_onChange(event) {
  console.log("input1 changed");

  /*  copre il caso:
        - due files sono già stati selezionati e convertiti
        - seleziono un nuovo file input1
        - allora resetto la selezione di input2
  */
  input2_form.reset();

  let reader = new FileReader();

    /*  impedisce che l'utente selezioni prima input2 e poi input1 disabilitando
        il secondo input finché il primo file non ha finito di caricare.

        assumo che il workflow desiderato sia sempre:
          - seleziono primo file
          - carico primo file
          - seleziono secondo file
          - converti non appena il secondo file ha finito di essere letto

          questo elimina anche lo scenario di una race-condition nel caso che il secondo file
          venisse letto più velocemente del primo, facendo partire la conversione prima che
          la lettura del primo file fosse ultimata.
    */
  reader.onload = (event) => {
    (input2.removeAttribute("disabled"));
    textarea1.value = event.target.result;
    console.log("input1 loaded")
  };

  let file = input1.files[0];
  console.log(file);
  reader.readAsText(file);
}

function input2_onChange(event) {
  console.log("input2 changed");

  let reader = new FileReader();

  reader.onload = (event) => {
    console.log("input2 loaded");
    textarea2.value = event.target.result;
    convert();
  }

  let file = input2.files[0];
  console.log(file);
  reader.readAsText(file);
}

function convert() {
  console.log("convert called")
  output.innerText = textarea1.value.toUpperCase() +
                      textarea2.value.toLowerCase();
}

console.log("### start ###");

let input1 = document.getElementById("input1");
input1.addEventListener("change", input1_onChange, false);

let input2 = document.getElementById("input2");
input2.addEventListener("change", input2_onChange, false);

let input2_form = document.getElementById("input2_form");

let textarea1 = document.getElementById("textarea1");
let textarea2 = document.getElementById("textarea2");
let output = document.getElementById("output");
